// import 'package:flappy_putin/views/credits_screen.dart';
import 'package:flappy_putin/views/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flappy_putin/views/start_screen.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const StartScreen(),
      initialRoute: '/',
      routes: {
        HomeScreen.routeName: (context) => const HomeScreen(),
        // CreditsScreen.routeName: (context) => const CreditsScreen(),
      },
    );
  }
}
