import 'package:flutter/material.dart';

class PutinHead extends StatelessWidget {
  const PutinHead({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90,
      height: 90,
      decoration: const BoxDecoration(
        // border: Border.all(
        //   width: 4,
        // ),
        image: DecorationImage(
          fit: BoxFit.contain,
          image: AssetImage('assets/images/head3.png'),
        ),
      ),
    );
  }
}
