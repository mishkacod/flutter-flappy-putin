import 'package:flutter/material.dart';

class MyBarrier extends StatelessWidget {
  final double size;
  final String image;

  const MyBarrier({Key? key, required this.size, required this.image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth / 4,
      height: size,
      decoration: BoxDecoration(
        // border: Border.all(
        //   width: 4,
        // ),
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage(image),
        ),
      ),
    );
  }
}
