import 'package:flutter/material.dart';

class MenuButton extends StatelessWidget {
  final double width;
  final String text;
  final Color textColor;
  final Color color;
  final VoidCallback onPress;

  const MenuButton({
    Key? key,
    required this.width,
    required this.text,
    required this.textColor,
    required this.color,
    required this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: width,
        height: 52,
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: color, // Background color
            ),
            child: Text(
              text,
              style: TextStyle(
                color: textColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: onPress));
  }
}
