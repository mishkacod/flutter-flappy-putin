class AssetName {
  static Audio audio = Audio();
}

class Audio {
  String dieOGG = 'audio/die.ogg';
  String hitOGG = 'audio/hit.ogg';
  String pointOGG = 'audio/point.ogg';
  String swooshOGG = 'audio/swoosh.ogg';
  String wingOGG = 'audio/wing.ogg';
  String dieWAV = 'audio/die.wav';
  String hitWAV = 'audio/hit.wav';
  String pointWAV = 'audio/point.wav';
  String swooshWAV = 'audio/swoosh.wav';
  String wingWAV = 'audio/wing.wav';
}
