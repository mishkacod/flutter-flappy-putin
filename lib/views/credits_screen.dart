import 'package:flutter/material.dart';

class CreditsScreen extends StatelessWidget {
  static const String routeName = '/creditsScreen';

  const CreditsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Team Credits'),
      ),
      body: Center(
        child: Column(
          children: const [
            Text('PTN PNH'),
          ],
        ),
      ),
    );
  }
}
