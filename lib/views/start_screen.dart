import 'package:flappy_putin/widgets/menu_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flappy_putin/views/credits_screen.dart';
import 'package:flappy_putin/views/home_screen.dart';

class StartScreen extends StatelessWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;
    final deviceWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      extendBody: true,
      extendBodyBehindAppBar: true,
      body: Container(
        height: deviceHeight,
        width: deviceWidth,
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              'assets/images/start_wallpaper3.jpeg',
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16.0,
            vertical: 8,
          ),
          child: Column(
            children: [
              const Spacer(),
              MenuButton(
                width: deviceWidth / 2,
                color: const Color.fromRGBO(233, 194, 134, 1),
                textColor: Colors.white,
                text: 'Почати',
                onPress: () {
                  Navigator.of(context).pushNamed(HomeScreen.routeName);
                },
              ),
              const SizedBox(
                height: 20,
              ),
              // MenuButton(
              //   width: deviceWidth,
              //   color: Colors.blue,
              //   textColor: Colors.white,
              //   text: 'Бали',
              //   onPress: () {
              //     Navigator.of(context).pushNamed(CreditsScreen.routeName);
              //   },
              // ),
              MenuButton(
                width: deviceWidth / 2,
                color: const Color.fromRGBO(233, 194, 134, 1),
                textColor: Colors.white,
                text: 'Вихід',
                onPress: () {
                  SystemNavigator.pop();
                },
              ),
              const SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
