import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

import 'package:flappy_putin/utils/asset_name.dart';
import 'package:flappy_putin/widgets/barriers.dart';
import 'package:flappy_putin/widgets/head.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/homePageScreen';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static const String highScoreKey = 'highScore';
  static double birdYAxis = 0;
  double time = 0;
  double height = 0;
  double initialHeight = birdYAxis;

  double upperbarrierOneHeight = 0.0;
  double lowerBarrierOneHeight = 0.0;

  double upperbarrierTwoHeight = 0.0;
  double lowerBarrierTwoHeight = 0.0;

  static double barrierXOne = -2.5;
  double barrierXTwo = barrierXOne + 1.75; // 0.75

  int score = 0;
  int highScore = 0;

  bool gameStarted = false;

  late Timer scoreTimer;

  late final AudioCache _audioCache;

  void setInitialValues() {
    setState(() {
      birdYAxis = 0;
      time = 0;
      height = 0;
      initialHeight = birdYAxis;
      barrierXOne = -2.5;
      barrierXTwo = barrierXOne + 1.75;
      score = 0;
    });
  }

  Future<void> getHighScore() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      highScore = prefs.getInt(highScoreKey) ?? 0;
    });
  }

  @override
  void initState() {
    super.initState();
    _audioCache = AudioCache(
      prefix: 'assets/',
      fixedPlayer: AudioPlayer()..setReleaseMode(ReleaseMode.STOP),
    );
    getHighScore();
    setInitialValues();
  }

  Future<void> showLoseDialog() async {
    await CoolAlert.show(
      context: context,
      type: CoolAlertType.warning,
      title: 'путин вмер',
      text: score <= 0 ? 'Слава Україні!' : 'Ви набрали $score очок',
      confirmBtnText: 'Ще',
      cancelBtnText: 'Вихід',
      confirmBtnColor: const Color.fromRGBO(233, 194, 134, 1),
      showCancelBtn: true,
      barrierDismissible: false,
      onConfirmBtnTap: () {
        Navigator.of(context).pop();
        setInitialValues();
      },
      onCancelBtnTap: () {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
    );
  }

  void jump() {
    setState(() {
      time = 0;
      initialHeight = birdYAxis;
    });
  }

  void startGame() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    gameStarted = true;
    setState(() {
      birdYAxis = 0;
      time = 0;
      height = 0;
      initialHeight = birdYAxis;
      barrierXOne = -2.5;
      barrierXTwo = barrierXOne + 1.75;
      score = 0;
    });
    scoreTimer = Timer.periodic(
      const Duration(seconds: 1),
      (timer) {
        if (gameStarted) {
          setState(() {
            score++;
          });
        }
      },
    );
    Timer.periodic(const Duration(milliseconds: 50), (timer) async {
      time += 0.05;
      height = -4.9 * time * time + 2.0 * time;
      setState(() {
        birdYAxis = initialHeight - height;
        if (barrierXOne > 2) {
          barrierXOne -= 3.5;
        } else {
          barrierXOne += 0.04;
        }
        if (barrierXTwo > 2) {
          barrierXTwo -= 3.5;
        } else {
          barrierXTwo += 0.04;
        }
      });
      if (birdYAxis > 1.1) {
        timer.cancel();
        scoreTimer.cancel();
        if (score > highScore) {
          highScore = score;
          await prefs.setInt(highScoreKey, highScore);
        }
        setState(() {});
        gameStarted = false;
      }

      if (barrierXOne >= -0.25 && barrierXOne <= 0.25) {
        if (birdYAxis <= -0.2 || birdYAxis >= 0.6) {
          timer.cancel();
          scoreTimer.cancel();
          gameStarted = false;
          _audioCache.play(AssetName.audio.hitOGG);
          if (score > highScore) {
            highScore = score;
            await prefs.setInt(highScoreKey, highScore);
          }
          setState(() {});
          await showLoseDialog();
        }
      }

      if (barrierXTwo >= -0.25 && barrierXTwo <= 0.25) {
        if (birdYAxis <= -0.6 || birdYAxis >= 0.2) {
          timer.cancel();
          scoreTimer.cancel();
          gameStarted = false;
          _audioCache.play(AssetName.audio.hitOGG);
          if (score > highScore) {
            highScore = score;
            await prefs.setInt(highScoreKey, highScore);
          }
          setState(() {});
          showLoseDialog();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;

    upperbarrierOneHeight = screenHeight / 2.5;
    lowerBarrierOneHeight = screenHeight / 5;

    upperbarrierTwoHeight = screenHeight / 5;
    lowerBarrierTwoHeight = screenHeight / 2.5;

    return GestureDetector(
      onTap: () async {
        if (gameStarted) {
          _audioCache.play(AssetName.audio.wingOGG);
          jump();
        } else {
          startGame();
        }
      },
      child: Scaffold(
        body: Stack(
          children: [
            AnimatedContainer(
              alignment: Alignment(0, birdYAxis),
              duration: const Duration(milliseconds: 0),
              child: const PutinHead(),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(
                    'assets/images/game_wallpaper2.jpeg',
                  ),
                ),
              ),
            ),
            gameStarted
                ? Container()
                : Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        SizedBox(
                          height: 120,
                        ),
                        Text(
                          'Жмакни для старту',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w900,
                            fontSize: 24.0,
                            letterSpacing: 8,
                          ),
                        ),
                      ],
                    ),
                  ),
            AnimatedContainer(
              alignment: Alignment(barrierXOne, -1.1),
              duration: const Duration(seconds: 0),
              child: MyBarrier(
                size: upperbarrierOneHeight,
                image: 'assets/images/rpg_down3.png',
              ),
            ),
            AnimatedContainer(
              alignment: Alignment(barrierXOne, 1.1),
              duration: const Duration(seconds: 0),
              child: MyBarrier(
                size: lowerBarrierOneHeight,
                image: 'assets/images/rpg_up3.png',
              ),
            ),
            AnimatedContainer(
              alignment: Alignment(barrierXTwo, -1.1),
              duration: const Duration(seconds: 0),
              child: MyBarrier(
                size: upperbarrierTwoHeight,
                image: 'assets/images/rpg_down3.png',
              ),
            ),
            AnimatedContainer(
              alignment: Alignment(barrierXTwo, 1.1),
              duration: const Duration(seconds: 0),
              child: MyBarrier(
                size: lowerBarrierTwoHeight,
                image: 'assets/images/rpg_up3.png',
              ),
            ),
            Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    height: 100,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                Text(
                                  score.toString(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 32,
                                  ),
                                ),
                                const Text(
                                  ' / ',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 32,
                                  ),
                                ),
                                Text(
                                  highScore.toString(),
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 32,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
